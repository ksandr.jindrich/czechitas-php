<!DOCTYPE html>
<html lang="cs">
	<head>
		<meta charset="utf-8">
		<title>Kryštůfek Robin a medvídek Pú</title>
	</head>
	<body>

		<h1>Web Kryštůfka Robina</h1>

		<p>Představím vám zde své <strong>dobré kamarády</strong>: medvídka Pú, Tygříka, Íjáčka a Prasátko.</p>

		<ul>
			<li><a href="index.html">Úvod</a></li>
			<li>Galerie</li>
			<li><a href="kontakt.html">Kontakt</a></li>
		</ul>

		<h2>Galerie</h2>

		<p>Společně se svými zvířecími kamarády zažíváme bláznivá dobrodružství.</p>

		<img src="obrazky/drak.jpg" alt="drak">
		<img src="obrazky/koupel.jpg" alt="koupel">
		<img src="obrazky/oslik.jpg" alt="Oslík">
		<img src="obrazky/oslik-prasatko.jpg" alt="Oslík a Prasátko">
		<img src="obrazky/kralik.jpg" alt="Králík">
		<img src="obrazky/protivitr.jpg" alt="protivítr">
		<img src="obrazky/pu-koupelna.jpg" alt="Pů v koupelně">
		<img src="obrazky/pu-robin.jpg" alt="Pů a Robin">
		<img src="obrazky/stopy.jpg" alt="stopy">

		<p>Pro <a href="http://www.czechitas.cz/">Czechitas</a> s&nbsp;láskou vyrobili Kryštůfek Robin a Medvídek Pú</p>
		<p>Autorem skenů je <a href="https://www.flickr.com/photos/bibliodyssey/3066814584">Paul K.</a> pod licencí <a href="https://creativecommons.org/licenses/by/2.0/">CC 2.0</a></p>

	</body>
</html>


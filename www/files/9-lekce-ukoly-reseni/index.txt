<html>
    <head>
        <meta charset="UTF-8">
        <title>Kalkulačka</title>
    </head>
    <body>

		<?php require_once 'konfigurace.php'; ?>

		
		<?php if (!empty($_SESSION['vysledek'])) { ?>
			<p><?= $_SESSION['vysledek']; ?><p>
		<?php } else if (!empty($_SESSION['chyba'])) { ?>
			<p><?= $_SESSION['chyba']; ?></p>
		<?php } ?>
		
		<?php
		$cislo1 = (isset($_SESSION['cislo1']) ? $_SESSION['cislo1'] : "");
		$cislo2 = (isset($_SESSION['cislo2']) ? $_SESSION['cislo2'] : "");
		$operace = (isset($_SESSION['operace']) ? $_SESSION['operace'] : "")
		?>

		<form method="post" action="kalkulackaZpracovani.php">
			<label for="cislo1">Číslo 1</label>
			<input type="text" name="cislo1" id="cislo1" value="<?= $cislo1; ?>">   
			<br>
			<br>

			<label for="operace">Operace</label>
			<select name="operace" id="operace">
				<option value="">-- Vyberte operaci --</option>
				<option value="<?= ZNAK_SCITANI; ?>" <?= ($operace == ZNAK_SCITANI ? "selected" : ""); ?>>sčítání</option>
				<option value="<?= ZNAK_ODECITANI; ?>" <?= ($operace == ZNAK_ODECITANI ? "selected" : ""); ?>>odčítání</option>
				<option value="<?= ZNAK_NASOBENI; ?>" <?= ($operace == ZNAK_NASOBENI ? "selected" : ""); ?>>násobení</option>
				<option value="<?= ZNAK_DELENI; ?>" <?= ($operace == ZNAK_DELENI ? "selected" : ""); ?>>dělení</option>
			</select>
			<br>
			<br>

			<label for="cislo2">Číslo 2</label>
			<input type="text" name="cislo2" id="cislo2" value="<?= $cislo2; ?>"> 
			<br>
			<br>

            <input type="submit" value="Spočítej">

        </form>
			
		<?php session_unset(); ?>

	</body>
</html>
<?php

namespace CzechitasPHP;

/**
 * Třída pro obsluhu komponent v šabloně jako ne například menu.
 *
 * @author Jindřich Ksandr <ksandr.jindrich@gmail.com>
 */
class Element {

	/**
	 * Konstruktor třídy. Volá se vždy, když chceme vytvořit instanci třídy.
	 */
	public function __construct() {

	}

	/**
	 * Vykreslení menu.
	 * @param string vykreslená šablona menu
	 */
	public function renderMenu() {

		// pokud nejsou URL
		if (empty(App::$urls)) {
			return "";
		}

		// projdu všechny URL
		$data = array();
		foreach (App::$urls as $url => $row) {
			// pokud není nastaveno zobrazování URL v menu, pokračuji dále
			if (empty($row['showInMenu'])) {
				continue;
			}

			// naplnění daty
			$data[$url] = new \stdClass();
			$data[$url]->title = empty($row['titleMenu']) ? $row['title'] : $row['titleMenu'];
			$data[$url]->active = FALSE;

			// informace, že se na tomto URL nacházím
			if (App::$urlPath == $url) {
				$data[$url]->active = TRUE;
			}

			// pokud URL nemá děti, pokračuji dále
			if (empty($row['children'])) {
				continue;
			}

			// projdu všechny děti a zjistím, zda jsem na URL dítěte
			// pokud ano, označím URL jako aktivní
			foreach ($row['children'] as $childrenUrl => $children) {
				if (App::$urlPath == $childrenUrl) {
					$data[$url]->active = TRUE;
				}
			}
		}

		// naplnění dat pro šablonu
		$menu = new \stdClass();
		$menu->menu = $data;
		$menu->actualUrl = App::$urlPath;

		// zpracování a odchycení výstupu (z důvodů, abych mohl používat konstrukce PHP v šabloně)
		ob_start();
		include TEMPLATES_ELEMENTS_PATH . '/menu.phtml';
		return ob_get_clean();
	}

	/**
	 * Vykreslí podmenu.
	 * @param string $parentUrl
	 * @return string vykreslená šablona podmenu.
	 */
	public function renderSubmenu($parentUrl) {

		// pokud je vstupní parametr prázdný
		if (empty($parentUrl)) {
			return "";
		}

		// pokud URL nemá děti
		if (empty(App::$urls[$parentUrl]['children'])) {
			return "";
		}

		// projdu všechny děti zadané URL
		$data = array();
		foreach (App::$urls[$parentUrl]['children'] as $url => $row) {

			// naplnění daty
			$data[$url] = new \stdClass();
			$data[$url]->title = empty($row['titleMenu']) ? $row['title'] : $row['titleMenu'];
			$data[$url]->active = FALSE;

			// informace, že se na tomto URL nacházím
			if (App::$urlPath == $url) {
				$data[$url]->active = TRUE;
			}
		}

		// naplnění dat pro šablonu
		$menu = new \stdClass();
		$menu->menu = $data;
		$menu->actualUrl = App::$urlPath;

		// zpracování a odchycení výstupu (z důvodů, abych mohl používat konstrukce PHP v šabloně)
		ob_start();
		include TEMPLATES_ELEMENTS_PATH . '/submenu.phtml';
		return ob_get_clean();
	}

}

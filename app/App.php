<?php

namespace CzechitasPHP;

/**
 * Třída pro obsluhu tohoto webu.
 *
 * @author Jindřich Ksandr <ksandr.jindrich@gmail.com>
 */
class App {

	// konstanty
	const LAYOUT = "layout.phtml";
	const NOT_FOUND_URL = "/404";

	/** @var string URL adresa */
	public static $urlPath;

	/** @var string query string z URL adresy (to co je za ?) */
	public static $urlQuery;

	/** @var array všechny URL */
	public static $urls;

	/** @var string aktuálně načtená šablona */
	private $templateFile;

	/** @var \stdClass obsah pro šablonu */
	private $templateContent;

	/**
	 * Konstruktor třídy. Volá se vždy, když chceme vytvořit instanci třídy.
	 */
	public function __construct() {
		// načtu si URL adresu
		$url = parse_url(filter_input(INPUT_SERVER, 'REQUEST_URI'));
		self::$urlPath = $url['path'];
		self::$urlQuery = (isset($url['query']) ? $url['query'] : '');

		// zpracování souboru s URL adresami
		$json = "";
		if (file_exists(CONFIG_PATH . '/url.json')) {
			$json = file_get_contents(CONFIG_PATH . '/url.json');
		}

		self::$urls = json_decode($json, TRUE);

		// vytvoření objektu pro předávání dat do šablony
		$this->templateContent = new \stdClass();

		// načtení šablony
		$this->loadTemplate();
	}

	/**
	 * Načtení stránky a její výpis do prohlížeče.
	 */
	public function run() {
		// nadefinování proměnných, které poté používáme v šabloně
		$wwwDir = WWW_DIR;
		$pagePath = TEMPLATES_PAGE_PATH;

		// objekt pro vykreslování komponent
		require_once APP_PATH . '/Element.php';
		$element = new Element();

		// obsah pro šablonu
		$titleSeo = "Dlouhodobý kurz jazyka PHP s Czechitas";
		if (!empty($this->templateContent->titleSeo)) {
			$titleSeo = "{$this->templateContent->titleSeo} - {$titleSeo}";
		}
		$this->templateContent->titleSeo = $titleSeo;

		$template = $this->templateContent;

		// zpracování a odchycení výstupu (z důvodů, abych mohl používat konstrukce PHP v šabloně)
		ob_start();
		include $this->templateFile;
		$template->content = ob_get_clean();

		include_once TEMPLATES_PATH . '/' . self::LAYOUT;
	}

	/**
	 * Načtení šablony podle URL.
	 * @param array $data
	 */
	private function loadTemplate() {

		// nejdříve zjistím, zda exituje URL
		if (!empty(self::$urls[self::$urlPath])) {

			// poté zjistím, zda exituje šablona
			if (!empty(self::$urls[self::$urlPath]['template'])) {
				$template = TEMPLATES_PAGE_PATH . "/" . self::$urls[self::$urlPath]['template'];
				$templateData = self::$urls[self::$urlPath];
			}
		} else {
			// pokud neexistuje URL v hlavní větvi, podívam se jestli není v jeho dětech
			foreach (self::$urls as $item) {
				// když nejsou děti, jdu hned na další prvek
				if (empty($item['children'])) {
					continue;
				}

				// pokud existuje šablona, nastavím ji a ukončím cyklus
				if (!empty($item['children'][self::$urlPath]['template'])) {
					$template = TEMPLATES_PAGE_PATH . "/" . $item['children'][self::$urlPath]['template'];
					$templateData = $item['children'][self::$urlPath];
					break;
				}
			}
		}

		// pokud neexituje URL nebo neexistuje soubor se šablonou
		if (empty($templateData) || !file_exists($template)) {
			$template = TEMPLATES_PAGE_PATH . "/" . self::$urls[self::NOT_FOUND_URL]['template'];
			$templateData = self::$urls[self::NOT_FOUND_URL];
		}

		$this->templateFile = $template;
		$this->templateContent->title = $templateData['title'];
		$this->templateContent->titleSeo = isset($templateData['titleSeo']) ? $templateData['titleSeo'] : $templateData['title'];
	}

}

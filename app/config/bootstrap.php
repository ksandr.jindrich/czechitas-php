<?php

// definování cest, které se používají v rámci tohoto webu
define('WWW_PATH', ROOT_PATH . '/www');
define('WWW_DIR', '/www');
define('LIBS_PATH', ROOT_PATH . '/libs');
define('TEMPLATES_PATH', WWW_PATH . '/templates');
define('TEMPLATES_PAGE_PATH', TEMPLATES_PATH . '/page');
define('TEMPLATES_ELEMENTS_PATH', TEMPLATES_PATH .'/_elements');

// načtená a spuštění knihovny, která mi pomáhá zachytávat a odhalovat chyby v programování
require_once LIBS_PATH . '/tracy/tracy.phar';
Tracy\Debugger::enable();
Tracy\Debugger::$strictMode = TRUE;
<?php

// definice pro uložení cesty ke složce, kde je tento web
define('ROOT_PATH', __DIR__);

// definice pro uložení cesty ke složce, kde jsem si uložil soubory ovládající tento web
define('APP_PATH', ROOT_PATH . '/app');
define('CONFIG_PATH', APP_PATH . '/config');

// načtení zaváděcího souboru tohoto webu
require_once CONFIG_PATH . '/bootstrap.php';

// načtení souboru pro spuštění aplikace
require_once APP_PATH . '/App.php';

// vytvoření instance
$app = new \CzechitasPHP\App();

// zavolání funkce, která "spustí" tento web
$app->run();
